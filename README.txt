ASX
----------------------------------------
This module scrapes share price data from ASX website.

INSTALLATION
----------------------------------------
1. Copy the module as normal.
2. Enable the module from the module administration page.
3. Configure the module (see "Configuration" below).

CONFIGURATION
----------------------------------------
1. Configure ASX settings at admin/config/services/asx
2. Configure ASX permissions at admin/people/permissions

CONTACT
----------------------------------------
The current maintainer is Ashish Upadhayay <contact@ashish.com.au>

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue.
